﻿using System.Collections.Generic;
using System.IO;

namespace Pathfinding {

    /// <summary>
    /// Dijkstra's algorithm implemented according to pseudocode from "Artificial Intelligence for Games, 2nd edition" (Millington, Funge)
    /// </summary>
    public class Dijkstra {

        public static IEnumerable<IConnection> PathfindDijkstra(IGraph graph, INode start, INode end)
        {
            // Initialize the record for the start node
            var startRecord = new NodeRecord(start, null, 0);

            // Initialize the open and closed lists
            var open = new List<NodeRecord>();
            open.Add(startRecord);
            var closed = new List<NodeRecord>();

            // Iterate through processing each node
            while (open.Count > 0) {
                // Find the smallest element in the open list
                open.Sort();
                var current = open[0];

                // If it is the goal node, return it
                if (current.Node == end) break;

                // Otherwise get its outgoing connections
                var connections = graph.GetConnections(current.Node);

                // Loop through each connection
                foreach (IConnection connection in connections) {
                    // Get the cost estimate for the end node
                    var endNode = connection.ToNode;
                    var endNodeCost = current.CostSoFar + connection.Cost;

                    // Skip if the node is closed
                    if (closed.Exists(record => record.Node == endNode)) continue;

                    var endNodeRecord = new NodeRecord();
                    // Or if its open and we've found a worse route
                    if (open.Exists(record => record.Node == endNode)) {
                        // Find the record in the open list corresponding to the endNode
                        endNodeRecord =
                            open.Find(record => record.Node == endNode);
                        if (endNodeRecord.CostSoFar <= endNodeCost)
                            continue;
                    }
                    else {
                        // Otherwise we've got an unvisited node, so make a record for it
                        endNodeRecord.Node = endNode;
                    }

                    // We're here if we need to update the node
                    // Update the cost and connection
                    endNodeRecord.CostSoFar = endNodeCost;
                    endNodeRecord.Connection = connection;

                    // Add it to the open list
                    open.Add(endNodeRecord);

                    // We've finished looking at the connections for the current node,
                    // so add it to the closed list and remove it from the open list
                    open.Remove(current);
                    closed.Add(current);

                }

                // We're here if we've either found the goal, or
                // if we've no more nodes to search, find which
                if (current.Node != end) {
                    // We've run out of nodes without finding the goal
                    // so there's no solution
                    return null;
                }
                else {
                    // Compile the list of connections in the path
                    var path = new List<IConnection>();
                    
                    // Sort the closed list so that we find the least cost connections first
                    closed.Sort();
                    
                    // Work back along the path
                    // accumulating connections
                    while (current.Node != start) {
                        path.Add(current.Connection);
                        current =
                            closed.Find(
                                record =>
                                    record.Connection.FromNode ==
                                        current.Connection.FromNode);
                    }

                    // Reverse the path and return it
                    path.Reverse();
                    return path;
                }
            }

            return null;
        }

    }

}
