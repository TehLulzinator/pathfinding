﻿using System;

namespace Pathfinding
{
    internal struct NodeRecord : IComparable<NodeRecord>
    {
        public INode Node;
        public IConnection Connection;
        public uint CostSoFar;

        public NodeRecord(INode node, IConnection connection, uint costSoFar)
        {
            Node = node;
            Connection = connection;
            CostSoFar = costSoFar;
        }

        public int CompareTo(NodeRecord other)
        {
            return CostSoFar.CompareTo(other.CostSoFar);
        }

    }
}