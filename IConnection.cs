﻿namespace Pathfinding
{
    public interface IConnection
    {
        /// <summary>
        /// Returns the non-negative cost of the connection.
        /// </summary>
        uint Cost { get; }

        /// <summary>
        /// Returns the node that this connection came from.
        /// </summary>
        INode FromNode { get; }

        /// <summary>
        /// Returns the node that this connection leads to.
        /// </summary>
        INode ToNode { get; }

    }
}
