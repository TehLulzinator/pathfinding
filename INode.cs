﻿using System.Collections.Generic;

namespace Pathfinding
{
    public interface INode
    {
        /// <summary>
        /// Returns the connections outgoing from this node.
        /// </summary>
        IEnumerable<IConnection> Connections { get; }
    }
}