﻿using System.Collections.Generic;

namespace Pathfinding
{
    public interface IGraph
    {
        /// <summary>
        /// Returns the connections outgoing from the given node.
        /// </summary>
        /// <param name="fromNode"></param>
        /// <returns></returns>
        IEnumerable<IConnection> GetConnections(INode fromNode);
    }
}